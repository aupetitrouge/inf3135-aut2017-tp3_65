# Guide de contribution au projet

## Soumettre des changements

Pour soumettre une requête d'intégration, créez une branche de fonctionnalité basé sur master,
puis envoyez la requête d'intégration à la branche master du dépôt principal.
Celle-ci sera intégrée si elle est approuvée par deux développeurs et qu'elle passe les tests d'intégration.

Les conditions à respecter sont:
 - Chaque commit ne doit modifier qu'un seule chose.
 - Une requête d'intégration doit contenir des commits fonctionnellement liés.
 - Si possible, l'ajout ou la modification de fonctionalité doit être accompagnée de tests couvrant les changements.

## Guides de style

### Message de commits
 - Sont rédigés en anglais.
 - Débutent par une majuscule et se termine par un point.
 - Utilisent le temps impératif présent (ex: "Add x." pas "Adds x." ni "Added x.").
 - Sont préfixés par le nom du fichier sans extension si un seul fichier est modifié (ex: "Makefile: Do something.").

### Style du code
 - Tout est rédigé en anglais.
 - Pour la documentation des fonctions: utiliser le standard [Javadoc](http://www.oracle.com/technetwork/articles/java/index-137868.html).
 - Pour tout le reste: suivre les principes dictés par le [Guide de style du noyau Linux](https://www.kernel.org/doc/html/latest/process/coding-style.html).

### Style de la documentation externe
 - Tout est rédigé en français.
 - Respecte le format [Markdown](https://daringfireball.net/projects/markdown/).

### Style des requêtes d'intégration
 - Tout est rédigé en français.
